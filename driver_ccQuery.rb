#!/usr/local/bin/ruby -w
# Copyright 2018 Craig Mcphee


# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# NOTE: USE limit option unless you want HUGE download
# e.g. -d amazon.com/* gives ~14000 json results = ~14000 warc downloads!

require_relative 'ccQuery'
require 'optparse'

options = {}
OptionParser.new do |op|

  op.banner = "Usage: driver_ccQuery.rb [options]"

  options[:limit] = -1
  op.on('-l', '--limit LIMIT', "set limit on number of records to retrive") do |lim|
    options[:limit] = lim
  end

  options[:domain] = "example.com"
  op.on('-d', '--domain DOMAIN', 'Set required DOMAIN for search e.g amazon.com/*') do |d|
    options[:domain] = d
  end

  options[:help] = true
  op.on('-h', '--help', "Print help and usage info") {  puts op; exit }

  options[:warc] = "results.txt"
  op.on('-O', '--output FILENAME', 'Set output filename for retrieved warc data, default: results.txt') do |o|
    options[:warc] = o
  end
end.parse!

reader = CCWeb.new(options)

reader.query_single_domain(CCWeb::DATA_SETS[0])
reader.output_json_all  # raw json data in ruby hash format

reader.get_all_data     # get archive data corresponding to json index info
reader.output_warc_all  # dump all data into single file
reader.output_warc_sep  # output warc header, html header and html body into different files
