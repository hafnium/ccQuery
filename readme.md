## Synopsis

Code implements a lightweight Ruby based class for query and extraction of Common Crawl data via their web interface. Two files are given, the base class and an example driver. The Ruby code can export both the hased
json query data and the data extracted from the warc indexes. Only the required data is downloaded and decompressed, no additonal download overhead is required. 

## Code Example

Basic usage (Unix) ./driver\_ccQuery.rb -l 10 -d amazon.com/* -O amazon.txt 

This will query Common Crawl for all data from all paths for the domain amazon.com, outputting:
* amazon.txt - all decompressed warc data
* warc_h_amazon.txt - warc headers only
* html_h_amazon.txt - html headers only
* html_b_amazon.txt - html bodies only

Options are:
* -l n : limit retrived warc data to n entities
* -O NAME : output file name
* -d Domain : required domain including any wildcards

## Motivation

Wanted a Ruby based client to test some natural languge processing against crawled data.

## Installation

git clone, only one class, pretty straightforward.

## API Reference

Just check the public methods and the driver file.

## Contributors


## License

MIT :


 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
