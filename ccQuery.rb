# ccQuery.rb : Interogate common crawl dataset in s3 for url records for single domain
# Methods are all within namespace CommonCrawlWeb
# Copyright 2018 Craig McPhee

# checkout: http://index.commoncrawl.org/
# further info:
# https://github.com/webrecorder/pywb/wiki/CDX-Server-API#api-reference 
# https://www.bellingcat.com/resources/2015/08/13/using-python-to-mine-common-crawl/

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Moniker / nick:

#      Cadre          Cad        adreCad     reCadreCad    dreCadreCad    
#    adreCadreC      dreCa       eCadreCad   adreCadreCa   CadreCadreCa   
#   reC   eCadr      Cadre       dreCadreCa   Cad    dre   reCa    adre   
#  Cad     reCa     dreCad       Cadr   dreC  reCadreCad    dre           
#  reC              Cad eCa       eCa   Cadr  adreCadre     CadreC        
#  ad              dreCadreC      dre    eCa  eCadreCa      reCadre       
#  eC              CadreCadr      Cad   adre  dreCadreC     adreCad       
#  dr       ad    dreC   eCad    dreC  reCad  Cad   adr  a  eCa           
#  Cad     reC    Cad     reC   eCadreCadreC  reC   eCadre  dreC    Cad   
#   eCadreCadr  adreCa   CadreC dreCadreCad  Cadre  dreCad eCadreC dreC   
#    reCadre    eCadre   reCadr CadreCadr    reCad   adre  dreCadreCadr   


require 'net/http'
require 'zlib'
require 'stringio'
require 'json'
require 'timeout'

class CCWeb
  attr_reader :json_data, :results

  # DATA_SETS : list of possible crawling indexes: http://index.commoncrawl.org/
  DATA_SETS = %w[2018-13  2018-09  2018-05  2017-51  2017-47  2017-43  2017-39]

  # SEARCH_OPTS : possible options for cdx server queries
  SEARCH_OPTS = {
    :url        => "url=",        # Required: domain to return captures for
    :from       => "from=",       # Date results from
    :to         => "to=",         # Date results to
    :matchType  => "matchType=",  # either : exact, prefix, host, domain
    :limit      => "limit=",      # Limit number of returned results
    :sort       => "sort=",       # change order of results
    :output     => "output=",     # Results format: text - whatever underling uses (default), json - recommended
    :filter     => "filter=",     # filter for specific results fields
    :fl         => "fl=",         # Specify which output fields to return
  } 

  # timeout limit for http GET request
  TIMER = 10

  def initialize(options)
      @json_data  = [] # Parsed json data for domain
      @results    = []   # Actual decompressed warc data
      @filename   = options[:warc]
      @limit      = options[:limit]
      @domain     = options[:domain]
  end
  
  # Attempt domain query, return results as array of hashes
  def query_single_domain(data_set)

    prefix        = "http://index.commoncrawl.org/CC-MAIN-#{data_set}-index?"
    search_string = "#{SEARCH_OPTS[:url]}#{@domain}&#{SEARCH_OPTS[:output]}json"

    uri           = URI.parse(prefix + search_string)
    resp          = Net::HTTP.get_response(uri)
    
    case resp
      when Net::HTTPSuccess
        @json_data = format_results(resp.body)
      else
        @json_data = nil
    end
  end

  def get_all_data
    @json_data.each_with_index do |json_hash, i|
      return if i == @limit.to_i
      download_individual_data(json_hash)
    end
  end

  def output_warc_all
    File.open(@filename, "w") do |f|
      @results.each do |elem|
          f.puts elem
      end
    end
  end

  def output_warc_sep
    f1 = File.open("warc_h_#{@filename}", "w")
    f2 = File.open("html_h_#{@filename}", "w")
    f3 = File.open("html_b_#{@filename}", "w")
    @results.each do |data|
      w, hh, hb  = data.split(/\r\n\r\n/, 3)
      f1.puts w  + "\r\n\r\n"
      f2.puts hh + "\r\n\r\n"
      f3.puts hb + "\r\n\r\n"
    end
    f1.close
    f2.close
    f3.close
  end

  def output_json_all
    File.open("json_#{@filename}", "w") do |f|
      @json_data.each do |elem|
          f.puts elem
      end
    end
  end

private
  # download, decompress and return warc archive for given file
  def download_individual_data(details = {})
    return nil if details == nil
    prefix = "http://commoncrawl.s3.amazonaws.com/"
    url = URI.parse(prefix + details["filename"])
    resp = get_json_response(url, details)
   
    case resp
      when Net::HTTPSuccess
        @results.append decompress_data(resp.body)
      else
        @results.append ""
    end
  end


  def get_json_response(url, details = {})
      resp = nil
    begin
      Timeout.timeout(TIMER) do
        req = Net::HTTP::Get.new(url.path)
        req.add_field("Range", "bytes=#{details["offset"]}-#{details["offset"].to_i+details["length"].to_i-1}")
        Net::HTTP.start(url.host, url.port) do |http|
            resp = http.request(req)
        end
      end
    rescue Timeout::Error
        STDERR.puts "error: timeout limit reached:\t"
        return nil
    end
    return resp
  end

  def format_results(resp)
    data = resp.split("\n")
    output = []
    data.each do |elem|
      output.append JSON.parse(elem)
    end
    return output
  end


  def decompress_data(zip_data)
    io = StringIO.new(zip_data) 
    gz = Zlib::GzipReader.new(io)
    gz.read
  end

end

